from argparse import ArgumentParser
from dotenv import load_dotenv
from overpy import Overpass, RelationNode
from os import environ
from os.path import join, dirname

import requests as r
import matplotlib.pyplot as plt

load_dotenv(join(dirname(__file__), ".env"))
WS_API_KEY = environ.get("WS_API_KEY")

p = ArgumentParser()
p.add_argument("--line", help="OpenStreetMap relation ID of the line", required=True)
p.add_argument("--line-name", help="Name of the line", required=True)
p.add_argument("--full-ylim", help="Whether the Y-axis limits should be 0-100 or be based on the data", action="store_true")
args = p.parse_args()

api = Overpass()

valid_roles = ["stop", "stop_entry_only", "stop_exit_only"]

def ws_get(lat, lon):
    params = {
        "format": "json",
        "lat": lat,
        "lon": lon,
        "wsapikey": WS_API_KEY
    }
    req = r.get("https://api.walkscore.com/score", params=params)
    if req.json()["status"] == 2:
        return None
    return req.json()["walkscore"]

def get_relation(id):
    return api.query(f"[out:json];relation({id});out;")

def get_stops(relation_id):
    line = get_relation(relation_id).get_relations()[0]
    stop_nodes = list(filter(
        lambda x: isinstance(x, RelationNode) and x.role in valid_roles,
        line.members
    ))

    stops = []
    for node in stop_nodes:
        stop = api.query(f"[out:json];node({node.ref});out;").get_nodes()[0]
        stops.append((
            stop.tags["name"].replace(" station", ""), stop.lat, stop.lon))
    return stops

walkscores = []

print("Getting walkscores...")
for stop in get_stops(args.line):
    print(f"{stop[0]} ({stop[1]}, {stop[2]})", end="... ")
    ws = ws_get(float(stop[1]), float(stop[2]))
    walkscores.append((stop[0], 0 if ws is None else ws))
    print("no walkscore" if ws is None else "done")

print("Plotting graph...", end="")
plt.plot([i[0] for i in walkscores], [i[1] for i in walkscores])
plt.title(args.line_name)
plt.xlabel("Stations")
plt.ylabel("Walkscore")
if args.full_ylim:
    plt.ylim([0, 100])
plt.xticks(rotation=45, ha="right", rotation_mode="anchor")
plt.tight_layout()
plt.savefig(f"{args.line_name.lower().replace(' ', '_')}_walkscore.png")
print("done")
